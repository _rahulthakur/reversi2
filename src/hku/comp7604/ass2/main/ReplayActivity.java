
package hku.comp7604.ass2.main;

import hku.comp7604.ass2.main.widget.ReplayFragment;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.NavUtils;
import android.util.Log;

import com.actionbarsherlock.app.SherlockFragmentActivity;
import com.actionbarsherlock.view.MenuItem;

/**
 * Made for COMP7604 Game Design and Development, Assignment 2
 * 
 * @author Rahul Thakur
 */
public class ReplayActivity extends SherlockFragmentActivity {

	private static final String TAG = "ReplayActivity";
	public static final String EXTRA_GAME_ID = "gameId";

	@Override
	protected void onCreate(Bundle savedInstanceState) {

		super.onCreate(savedInstanceState);
		if (getSupportActionBar() != null) {
			getSupportActionBar().setDisplayHomeAsUpEnabled(true);
		}
		Intent extra = getIntent();
		if (extra == null) {
			Log.e(TAG, "No extra");
			finish();
			return;
		}
		FragmentManager fm = getSupportFragmentManager();
		if (fm.findFragmentById(android.R.id.content) == null) {
			ReplayFragment content = ReplayFragment.newInstance(extra.getLongExtra(EXTRA_GAME_ID, -1));
			fm.beginTransaction().add(android.R.id.content, content).commit();
		}
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {

		switch (item.getItemId()) {
			case android.R.id.home:
				NavUtils.navigateUpFromSameTask(this);
				return true;
		}
		return super.onOptionsItemSelected(item);
	}
}