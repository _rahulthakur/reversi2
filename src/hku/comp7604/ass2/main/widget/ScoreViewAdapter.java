
package hku.comp7604.ass2.main.widget;

import hku.comp7604.ass2.main.model.Cell;
import hku.comp7604.ass2.main.model.board.Board;
import hku.comp7604.ass2.main.model.board.Board.Callback;
import android.widget.ImageButton;
import android.widget.TextView;

/**
 * Made for COMP7604 Game Design and Development, Assignment 2
 * 
 * @author Rahul Thakur
 */
public class ScoreViewAdapter implements Callback {

	private TextView mScoreWhite;
	private TextView mScoreBlack;
	private ImageButton mPlayerWhite;
	private ImageButton mPlayerBlack;

	public void setupWidgets(TextView scoreWhite, TextView scoreBlack, ImageButton playerWhite, ImageButton playerBlack) {

		mScoreWhite = scoreWhite;
		mScoreBlack = scoreBlack;
		mPlayerWhite = playerWhite;
		mPlayerBlack = playerBlack;
	}

	private void setPlayerView(int currentPlayer) {

		if (currentPlayer == Board.BLACK) {
			mPlayerBlack.setEnabled(true);
			mPlayerWhite.setEnabled(false);
		} else {
			mPlayerBlack.setEnabled(false);
			mPlayerWhite.setEnabled(true);
		}
	}

	private void setScoreView(int scoreBlack, int scoreWhite) {

		mScoreWhite.setText("" + scoreWhite);
		mScoreBlack.setText("" + scoreBlack);
	}

	public void init(Board board) {

		setScoreView(board.getScoreBlack(), board.getScoreWhite());
		setPlayerView(board.currentPlayer());
		board.addCallbackListener(this);
	}

	@Override
	public void onNextPlayer(int nextPlayer) {

	}

	@Override
	public void onBoardUpdate(Board board) {

		setPlayerView(board.currentPlayer());
		setScoreView(board.getScoreBlack(), board.getScoreWhite());
	}

	@Override
	public void onGameEnd(Board board) {

	}

	@Override
	public void onCellUndo(Cell cell, int kind) {

	}
}
