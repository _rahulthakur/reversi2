
package hku.comp7604.ass2.main.widget;

import hku.comp7604.ass2.main.R;
import hku.comp7604.ass2.main.ReplayActivity;
import hku.comp7604.ass2.main.model.history.HistoryContract;
import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.CursorLoader;
import android.support.v4.content.Loader;
import android.support.v4.widget.SimpleCursorAdapter;
import android.view.View;
import android.widget.ListView;

import com.actionbarsherlock.app.SherlockListFragment;

/**
 * Made for COMP7604 Game Design and Development, Assignment 2
 * 
 * @author Rahul Thakur
 */
public class HistoryListFragment extends SherlockListFragment implements LoaderManager.LoaderCallbacks<Cursor> {

	SimpleCursorAdapter mAdapter;

	@Override
	public void onActivityCreated(Bundle savedInstanceState) {

		super.onActivityCreated(savedInstanceState);
		setEmptyText(getActivity().getString(R.string.no_games_in_history));
		setHasOptionsMenu(true);
		mAdapter = new HistoryCursorAdapter(getActivity());
		setListAdapter(mAdapter);
		getLoaderManager().initLoader(0, null, this);
	}

	@Override
	public Loader<Cursor> onCreateLoader(int id, Bundle args) {

		String selection = HistoryContract.HistoryColumns.NUMBEROFMOVES + ">?";
		String[] selectionArgs = new String[] { "0" };
		return new CursorLoader(getActivity(), HistoryContract.History.CONTENT_URI, null, selection, selectionArgs, null);
	}

	@Override
	public void onLoadFinished(Loader<Cursor> loader, Cursor data) {

		mAdapter.swapCursor(data);
	}

	@Override
	public void onLoaderReset(Loader<Cursor> loader) {

		mAdapter.swapCursor(null);
	}

	@Override
	public void onListItemClick(ListView l, View v, int position, long id) {

		Intent replayIntent = new Intent(getActivity(), ReplayActivity.class);
		replayIntent.putExtra(ReplayActivity.EXTRA_GAME_ID, id);
		startActivity(replayIntent);
	}
}