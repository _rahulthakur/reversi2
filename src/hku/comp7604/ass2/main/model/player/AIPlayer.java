
package hku.comp7604.ass2.main.model.player;

import hku.comp7604.ass2.main.model.Cell;
import hku.comp7604.ass2.main.model.board.Board;

import java.util.ArrayList;

import android.os.Handler;

/**
 * Made for COMP7604 Game Design and Development, Assignment 2
 * 
 * @author Rahul Thakur
 */
public class AIPlayer extends AbstractPlayer {

	private Board mBoard;

	public AIPlayer(Board board) {

		mBoard = board;
	}

	@Override
	public void play() {

		Handler handler = new Handler();
		handler.postDelayed(new Runnable() {

			@Override
			public void run() {

				Cell cell = playDelayed();
				AIPlayer.this.notify(cell);
			}
		}, 300);
	}

	protected Cell playDelayed() {

		Cell ret = new Cell(0, 0);
		ArrayList<Cell> allowd = mBoard.getAllowedMoves();
		int min = 0;
		for (Cell cell : allowd) {
			int temp = mBoard.checkNextMove(cell);
			if (temp > min) {
				min = temp;
				ret.x = cell.x;
				ret.y = cell.y;
			}
		}
		if (min != 0) {
			mBoard.move(ret);
			return ret;
		}
		return null;
	}
}
