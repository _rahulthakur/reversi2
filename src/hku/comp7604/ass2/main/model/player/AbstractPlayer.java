
package hku.comp7604.ass2.main.model.player;

import hku.comp7604.ass2.main.model.Cell;

/**
 * Made for COMP7604 Game Design and Development, Assignment 2
 * 
 * @author Rahul Thakur
 */
public abstract class AbstractPlayer implements Player {

	private OnPlayListener mOnPlayListener;

	@Override
	public void setOnPlayeListener(OnPlayListener listener) {

		mOnPlayListener = listener;
	}

	protected void notify(Cell cell) {

		if (mOnPlayListener != null) {
			mOnPlayListener.OnPlay(cell);
		}
	}
}
