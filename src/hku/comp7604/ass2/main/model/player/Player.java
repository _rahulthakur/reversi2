
package hku.comp7604.ass2.main.model.player;

import hku.comp7604.ass2.main.model.Cell;

/**
 * Made for COMP7604 Game Design and Development, Assignment 2
 * 
 * @author Rahul Thakur
 */
public interface Player {

	public interface OnPlayListener {

		void OnPlay(Cell cell);
	}

	public void setOnPlayeListener(OnPlayListener listener);

	public void play();
}