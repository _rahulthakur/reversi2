
package hku.comp7604.ass2.main.model;

import hku.comp7604.ass2.main.model.board.Board;
import android.os.Parcel;
import android.os.Parcelable;

/**
 * Made for COMP7604 Game Design and Development, Assignment 2
 * 
 * @author Rahul Thakur
 */
public class Cell implements Parcelable {

	public int x;
	public int y;
	public int contents;

	public Cell(int x, int y) {

		this.x = x;
		this.y = y;
		this.contents = Board.EMPTY;
	}

	//	public Cell(Parcel in) {
	//
	//		x = in.readInt();
	//		y = in.readInt();
	//		contents = in.readInt();
	//	}
	@Override
	public int describeContents() {

		return 0;
	}

	@Override
	public void writeToParcel(Parcel dest, int flags) {

		dest.writeInt(x);
		dest.writeInt(y);
		dest.writeInt(contents);
	}
}
