
package hku.comp7604.ass2.main.model.history;

import android.content.ContentProvider;
import android.content.ContentValues;
import android.content.UriMatcher;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteQueryBuilder;
import android.net.Uri;

/**
 * Made for COMP7604 Game Design and Development, Assignment 2
 * 
 * @author Rahul Thakur
 */
public class HistoryProvider extends ContentProvider {

	private static final int HISTORY = 1;
	private static final int HISTORY_ID = 2;
	private static final int GAME_ID = 10;
	public static final int GAME_ID_PATH_POSITION = 1;
	private static final UriMatcher sUriMatcher;
	static {
		sUriMatcher = new UriMatcher(UriMatcher.NO_MATCH);
		sUriMatcher.addURI(HistoryContract.CONTENT_AUTHORITY, "history", HISTORY);
		sUriMatcher.addURI(HistoryContract.CONTENT_AUTHORITY, "history/*", HISTORY_ID);
		sUriMatcher.addURI(HistoryContract.CONTENT_AUTHORITY, "game/*", GAME_ID);
	}
	private HistoryDatabase mOpenHelper;

	@Override
	public boolean onCreate() {

		mOpenHelper = new HistoryDatabase(getContext());
		return false;
	}

	@Override
	public Cursor query(Uri uri, String[] projection, String selection, String[] selectionArgs, String sortOrder) {

		final SQLiteDatabase db = mOpenHelper.getReadableDatabase();
		SQLiteQueryBuilder qb = new SQLiteQueryBuilder();
		String gameId;
		String orderBy = null;
		final int match = sUriMatcher.match(uri);
		switch (match) {
			case HISTORY: {
				qb.setTables(HistoryContract.Tables.HISTORY);
				qb.appendWhere(HistoryContract.History.DEFAULT_SELECTION);
				orderBy = HistoryContract.History.DEFAULT_SORT;
				break;
			}
			case HISTORY_ID: {
				qb.setTables(HistoryContract.Tables.HISTORY);
				gameId = uri.getPathSegments().get(GAME_ID_PATH_POSITION);
				qb.appendWhere(HistoryContract.History.buildHistoryIdSelection(gameId));
				break;
			}
			case GAME_ID: {
				qb.setTables(HistoryContract.Tables.GAME);
				gameId = uri.getPathSegments().get(GAME_ID_PATH_POSITION);
				qb.appendWhere(HistoryContract.Game.buildGameIdSelection(gameId));
				break;
			}
			default:
				throw new IllegalArgumentException("Unknown URI " + uri);
		}
		if (orderBy == null) {
			orderBy = sortOrder;
		}
		Cursor c = qb.query(db, projection, selection, selectionArgs, null, null, orderBy);
		c.setNotificationUri(getContext().getContentResolver(), uri);
		return c;
	}

	@Override
	public Uri insert(Uri uri, ContentValues values) {

		final SQLiteDatabase db = mOpenHelper.getWritableDatabase();
		final int match = sUriMatcher.match(uri);
		switch (match) {
			case HISTORY: {
				long gameId = db.insertOrThrow(HistoryContract.Tables.HISTORY, null, values);
				getContext().getContentResolver().notifyChange(uri, null, false);
				return HistoryContract.History.buildHistoryUri(gameId);
			}
			case GAME_ID: {
				db.insertOrThrow(HistoryContract.Tables.GAME, null, values);
				getContext().getContentResolver().notifyChange(uri, null, false);
				return HistoryContract.Game.buildGameUri(values.getAsLong(HistoryContract.GameColumns.REL));
			}
			default: {
				throw new UnsupportedOperationException("Unknown uri: " + uri);
			}
		}
	}

	@Override
	public int update(Uri uri, ContentValues values, String selection, String[] selectionArgs) {

		final SQLiteDatabase db = mOpenHelper.getWritableDatabase();
		final int match = sUriMatcher.match(uri);
		int retVal = 0;
		switch (match) {
			case HISTORY_ID: {
				retVal = db.update(HistoryContract.Tables.HISTORY, values, selection, selectionArgs);
				getContext().getContentResolver().notifyChange(uri, null, false);
				break;
			}
			default: {
				throw new UnsupportedOperationException("Unknown uri: " + uri);
			}
		}
		return retVal;
	}

	@Override
	public int delete(Uri uri, String selection, String[] selectionArgs) {

		throw new UnsupportedOperationException("Unknown uri: " + uri);
	}

	@Override
	public String getType(Uri uri) {

		final int match = sUriMatcher.match(uri);
		switch (match) {
			case HISTORY:
				return HistoryContract.History.CONTENT_TYPE;
			case HISTORY_ID:
				return HistoryContract.History.CONTENT_ITEM_TYPE;
			case GAME_ID:
				return HistoryContract.History.CONTENT_ITEM_TYPE;
			default:
				throw new UnsupportedOperationException("Unknown uri: " + uri);
		}
	}
}