
package hku.comp7604.ass2.main.model;

/**
 * Made for COMP7604 Game Design and Development, Assignment 2
 * 
 * @author Rahul Thakur
 */
public class Game {

	public int ID;
	public int date;
	public int scoreBlack;
	public int scoreWhite;
}
